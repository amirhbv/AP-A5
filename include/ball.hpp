#ifndef __BALL_H__
#define __BALL_H__

#include <cmath>

#include "../rsdl/rsdl.hpp"

#include "gameDefinedValues.hpp"

class Ball
{
public:
	Ball() {state = INSIDE; };
	void setPos(int x, int y, int angle = 90);
	int getX() {return xPos; };
	int getY() {return yPos; };
	int getVX() {return vx; };
	int getVY() {return vy; };
	void draw(Window *win);
	void drawFireball(Window *win);
	void move();
	bool getState() {return state; };
	void setState(bool _state) {state = _state; };
	void reflectVY() {vy *= (-1); };
	void reflectVX() {vx *= (-1); };
private:
	int vx, vy;
	int xPos, yPos;
	bool state;
};
#endif