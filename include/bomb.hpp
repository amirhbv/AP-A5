#ifndef __BOMB_H__
#define __BOMB_H__

#include "gameObject.hpp"

#include "gameDefinedValues.hpp"

class Bomb : public GameObject
{
public:
	Bomb() {};
	int type() { return MAP_BOMB; };
private:
	void draw(Window *win, int column, int row);
};
#endif