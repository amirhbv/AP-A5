#ifndef __BRICK_H__
#define __BRICK_H__

#include "gameObject.hpp"
#include "ball.hpp"

#include "gameDefinedValues.hpp"

class Brick : public GameObject
{
public:
	Brick(int _resistance = 0);
	int type() { return MAP_BRICK; };
	void hitBrick(Ball *ball);
	void crack();
	int getResistance() {return resistance; };
private:
	int red, green, blue;
	int resistance;
	void draw(Window *win, int column, int row);
	void setColor();
};
#endif
