#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "../rsdl/rsdl.hpp"
#include <string>

#include "definedValues.hpp"

class Button
{
public:
	Button(int topCornerX = 0, int topCornerY = 0, int _width = 0, int _height = 0, std::string _caption = "Button");
	void show(Window *win, RGB color);
	bool contains(int x, int y);
private:
	int topCornerX, topCornerY;
	int width;
	int height;
	std::string caption;
};
#endif