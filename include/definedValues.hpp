#define WINDOW_WIDTH 480
#define WINDOW_HEIGHT 640
#define WINDOW_TITLE "Swipe Brick Breakers"
#define TOP_MARGIN 100
#define LEFT_MARGIN 160
#define SPACE_BETWEEN 40
#define SPACE_BETWEEN_SCORES 50
#define BUTTON_WIDTH 160
#define BUTTON_HEIGHT 80
#define BUTTON_COLOR CYAN
#define SELECTED_BUTTON_COLOR RED
#define TICK_DURATION 40
#define IMG_BACKGROUND "assets/img/background.jpg"
#define SAVED_SCORE_FILENAME "highscores.txt"
#define BACK_SPACE 8
#define ENTER 13
#define ESC 27
#define RIGHT_ARROW 79
#define LEFT_ARROW 80
#define DOWN_ARROW 81
#define UP_ARROW 82
#define SCOREBOARD_MAXIMUM_NUMBER_OF_USERS 5
#define FONT_FREESANS "assets/font/OpenSans.ttf"
#define BUTTON_FONTSIZE 20
#define SETTING_FONTSIZE 30
#define GAMEOVER_FONTSIZE 50

#define	NEW_GAME 0
#define SCOREBOARD 1
#define SETTING 2
#define EXIT 3
#define MENU 4

#define SAVE 0
#define CANCEL 1

#define FALSE 0
#define TRUE 1
#define ZERO 0