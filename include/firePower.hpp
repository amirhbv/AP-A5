#ifndef __FIRE_POWER_H__
#define __FIRE_POWER_H__

#include "gameObject.hpp"

#include "gameDefinedValues.hpp"

class FirePower : public GameObject
{
public:
	FirePower() {};
	int type() { return MAP_FIRE_POWER; };
private:
	void draw(Window *win, int column, int row);
};
#endif