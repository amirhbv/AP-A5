#ifndef __GAME_H__
#define __GAME_H__

#include <ctime>
#include <vector>
#include <string>
#include <sstream>

#include "../rsdl/rsdl.hpp"
#include "ball.hpp"
#include "myString.hpp"
#include "gameObject.hpp"
#include "brick.hpp"
#include "bomb.hpp"
#include "lifeBall.hpp"
#include "firePower.hpp"

#include "gameDefinedValues.hpp"

class Game
{
public:
	Game(Window *win);
	~Game();
	void display(MyString *playerId, int highScore);
	int getScore() { return score; };
private:
	Window *win;
	MyString *playerId;
	int highScore;
	std::vector<Ball*> balls;
	bool mouseClick;
	bool firePower;
	bool isfirePowerOn;
	bool gameOver;
	bool shoot;
	int score;
	int angle;
	float ballX, ballY;
	GameObject *gameObjects[NUMBER_OF_COLUMNS][NUMBER_OF_ROWS];

	void draw();
	void drawHeader();
	void drawGameObjects();
	void drawBallAndArrow();
	void handleLastEvent();
	void handleMouseEvent();
	void handleKeyboardEvent();
	void handleGameObject();
	bool isGameOver();
	void moveGameObjectsDown();
	void addGameObjectToFirstRow();
	void addSpecialGameObjectsToFirstRow();
	void addBall();
	void shootFireBall();
	void shootBalls();
	void handleBallMove(Ball *ball);
	void handleFireBallMove(Ball *ball);
	void reflectFromWalls(Ball *ball);
	void hitBomb(int column, int row);
	int numberOfFreeCellInFirstRow();
	int calculateColumn(int x);
	int calculateRow(int y);
	std::string toString(int n);
};
#endif