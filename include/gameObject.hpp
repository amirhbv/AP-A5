#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__

#include <string>
#include <sstream>

#include "../rsdl/rsdl.hpp"

#include "gameDefinedValues.hpp"

class GameObject
{
public:
	GameObject() {};
	~GameObject() {};
	virtual int type() = 0;
	virtual void draw(Window *win, int column, int row) = 0;
protected:
	int xPos, yPos;
	virtual int calculateXPos(int column);
	virtual int calculateYPos(int row);
	std::string toString(int n);
};
#endif