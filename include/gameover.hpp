#ifndef __GAMEOVER_H__
#define __GAMEOVER_H__

#include <vector>
#include <sstream>

#include "../rsdl/rsdl.hpp"
#include "definedValues.hpp"
#include "button.hpp"
#include "user.hpp"

class GameOver
{
public:
	GameOver(Window *_win);
	int display(int score, int record);
private:
	Window *win;
	bool exit;
	std::vector<Button> buttons;
	unsigned int selectedButton;

	void addButtons();
	void draw(int score, int record);
	void drawButtons();
	void handleLastEvent();
	int chooseButton();
	std::string toString(int n);
};

#endif