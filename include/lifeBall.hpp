#ifndef __LIFE_BALL_H__
#define __LIFE_BALL_H__

#include "gameObject.hpp"

#include "gameDefinedValues.hpp"

class LifeBall : public GameObject
{
public:
	LifeBall() {};
	int type() { return MAP_LIFE_BALL; };
private:
	void draw(Window *win, int column, int row);
};
#endif