#ifndef __MENU_H__
#define __MENU_H__

#include <algorithm>
#include <fstream>
#include <vector>

#include "../rsdl/rsdl.hpp"
#include "scoreboard.hpp"
#include "myString.hpp"
#include "gameover.hpp"
#include "setting.hpp"
#include "button.hpp"
#include "game.hpp"
#include "user.hpp"

#include "definedValues.hpp"

class Menu
{
public:
	Menu();
	~Menu();
	void display();

private:
	Window *win;
	bool exit;
	std::vector<Button> buttons;
	unsigned int selectedButton;
	Scoreboard *scoreboard;
	Setting *setting;
	Game *game;
	GameOver *gameOver;
	std::vector<User> users;
	MyString *playerId;

	void addButtons();
	void draw();
	void drawButtons();
	void handleLastEvent();
	void handleMouseEvent();
	void handleKeyboardEvent();
	void chooseButton();
	void showScoreboard();
	void showSetting();
	void newGame();
	void showGameOver(int score);
	int  playerHighScore();
	void setPlayerHighScore(int score);
	void saveScores();
	void loadScores();
};

#endif