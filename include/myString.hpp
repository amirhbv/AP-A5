#ifndef __MY_STRING_H__
#define __MY_STRING_H__

#include <string>

#define MAX_STRING_LENGTH 18

class MyString
{
public:
	MyString(std::string input = "");
	void eraseLastChar();
	void addChar(char c);
	std::string getString();
	void setString(std::string input) {inputString = input; };
private:
	std::string inputString;
};
#endif
