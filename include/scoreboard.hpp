#ifndef __SCOREBOARD_H__
#define __SCOREBOARD_H__

#include <vector>
#include <sstream>

#include "../rsdl/rsdl.hpp"
#include "definedValues.hpp"
#include "button.hpp"
#include "user.hpp"

class Scoreboard
{
public:
	Scoreboard(Window *_win);
	void display(std::vector<User> users);
private:
	Window *win;
	bool exit;
	std::vector<Button> buttons;
	unsigned int selectedButton;

	void addButtons();
	void draw(std::vector<User> users);
	void drawButtons();
	void handleLastEvent();
	std::string toString(int n);
};

#endif