#ifndef __SETTING_H__
#define __SETTING_H__

#include <vector>

#include "../rsdl/rsdl.hpp"
#include "definedValues.hpp"
#include "myString.hpp"
#include "button.hpp"

class Setting
{
public:
	Setting(Window *_win);
	void setUser(MyString *_playerId) {playerId = _playerId;};
	void display();
private:
	Window *win;
	bool exit;
	std::vector<Button> buttons;
	unsigned int selectedButton;
	std::string oldUserName;
	MyString *playerId;

	void addButtons();
	void draw();
	void drawButtons();
	void handleLastEvent();
	void chooseButton();
};

#endif