#ifndef __USER_H__
#define __USER_H__

#include <string>
class User
{
public:
    User(std::string _id, int _highscore = 0);
    class Compare {
    public:
        Compare(std::string input) { id = input; };
        bool operator()(const User& user)const { return user.id == id; };
    private:
        std::string id;
    };
    bool operator < (const User& user) const {return highscore > user.highscore;};
    std::string getId() const {return id;};
    int getHighscore() const {return highscore;};
    void setHighscore(int _highScore);

private:
    std::string id;
    int highscore;
};

#endif