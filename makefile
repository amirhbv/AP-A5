UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	HEAD := \"sdlheaders/mac.hpp\"
  	CCFLAGS= -framework SDL2 -framework SDL2_image -framework SDL2_ttf
else
	HEAD := \"sdlheaders/linux.hpp\"
	CCFLAGS += -l SDL2 -l SDL2_image -l SDL2_ttf
endif

PROG = SwipeBrickBreakers.out
CC = g++
CPPFLAGS = -Wall -pedantic -std=c++98 -Iinclude
OBJS = $(patsubst src/%.cpp, obj/%.o, $(wildcard src/*.cpp))
OBJS += obj/rsdl.o

#.PHONY: all clean build run

build: header $(PROG)

run: build
	./$(PROG)

all: clean run

$(PROG): main.cpp $(OBJS)
	$(CC) $(CPPFLAGS) main.cpp $(OBJS) $(CCFLAGS)  -o $(PROG)

obj/rsdl.o: rsdl/rsdl.hpp rsdl/rsdl.cpp
	$(CC) $(CPPFLAGS) -c rsdl/rsdl.cpp -o obj/rsdl.o

obj/menu.o: include/menu.hpp src/menu.cpp
	$(CC) $(CPPFLAGS) -c src/menu.cpp -o obj/menu.o

obj/button.o: include/button.hpp src/button.cpp
	$(CC) $(CPPFLAGS) -c src/button.cpp -o obj/button.o

obj/user.o: include/user.hpp src/user.cpp
	$(CC) $(CPPFLAGS) -c src/user.cpp -o obj/user.o

obj/scoreboard.o: include/scoreboard.hpp src/scoreboard.cpp
	$(CC) $(CPPFLAGS) -c src/scoreboard.cpp -o obj/scoreboard.o

obj/setting.o: include/setting.hpp src/setting.cpp
	$(CC) $(CPPFLAGS) -c src/setting.cpp -o obj/setting.o

obj/game.o: include/game.hpp src/game.cpp
	$(CC) $(CPPFLAGS) -c src/game.cpp -o obj/game.o

obj/gameObject.o: include/gameObject.hpp src/gameObject.cpp
	$(CC) $(CPPFLAGS) -c src/gameObject.cpp -o obj/gameObject.o

obj/ball.o: include/ball.hpp src/ball.cpp
	$(CC) $(CPPFLAGS) -c src/ball.cpp -o obj/ball.o

obj/brick.o: include/brick.hpp src/brick.cpp
	$(CC) $(CPPFLAGS) -c src/brick.cpp -o obj/brick.o

obj/bomb.o: include/bomb.hpp src/bomb.cpp
	$(CC) $(CPPFLAGS) -c src/bomb.cpp -o obj/bomb.o

obj/lifeBall.o: include/lifeBall.hpp src/lifeBall.cpp
	$(CC) $(CPPFLAGS) -c src/lifeBall.cpp -o obj/lifeBall.o

obj/firePower.o: include/firePower.hpp src/firePower.cpp
	$(CC) $(CPPFLAGS) -c src/firePower.cpp -o obj/firePower.o

obj/gameover.o: include/gameover.hpp src/gameover.cpp
	$(CC) $(CPPFLAGS) -c src/gameover.cpp -o obj/gameover.o

obj/myString.o: include/myString.hpp src/myString.cpp
	$(CC) $(CPPFLAGS) -c src/myString.cpp -o obj/myString.o

header:
	mkdir -p obj
	echo "#include "$(HEAD) > ./rsdl/sdlHeaders.hpp
clean:
	rm -rf obj/*.o $(PROG)
