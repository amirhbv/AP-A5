#include "ball.hpp"

void Ball::setPos(int x, int y, int angle)
{
	xPos = x;
	yPos = y;
	vx = BALL_SPEED * cos(Radian(angle)) * (-1);
	vy = BALL_SPEED * sin(Radian(angle)) * (-1);
}

void Ball::move()
{
	xPos += vx;
	yPos += vy;
}

void Ball::draw(Window *win)
{
	win->draw_png(IMG_BALL, xPos - BALL_RADIUS / 2, yPos, BALL_RADIUS, BALL_RADIUS);
}

void Ball::drawFireball(Window *win)
{
	win->draw_png(IMG_FIRE_BALL, xPos - BALL_RADIUS / 2, yPos, BALL_RADIUS, BALL_RADIUS);
}