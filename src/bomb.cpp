#include "bomb.hpp"

void Bomb::draw(Window *win, int column, int row)
{
	xPos = calculateXPos(column);
	yPos = calculateYPos(row);
	win->draw_png(IMG_BOMB, xPos + MAP_CELL_WIDTH / 2 - BALL_RADIUS, yPos, BALL_RADIUS * 2, BALL_RADIUS * 2);
}