#include "brick.hpp"

using namespace std;

Brick::Brick(int _resistance)
{
	resistance = _resistance;
	blue = 255 - resistance * 20;
	while(blue < 0)
		blue += 255;	
	green = 0;
	red = 0;
}

void Brick::draw(Window *win, int column, int row)
{
	xPos = calculateXPos(column);
	yPos = calculateYPos(row);
	win->fill_rect(xPos, yPos, MAP_CELL_WIDTH, MAP_CELL_HEIGHT, RGB(red, green, blue));
	win->show_text(toString(resistance), xPos + MAP_CELL_WIDTH / 2 - toString(resistance).size() * 5, yPos + 5, RED, FONT_FREESANS, BUTTON_FONTSIZE);
}

void Brick::hitBrick(Ball *ball)
{
	int x = ball->getX();
	int y = ball->getY();
	if ( x > xPos && x < (xPos + MAP_CELL_WIDTH / 4) && (y > (yPos) && y < (yPos + MAP_CELL_HEIGHT)) && ball->getVX() > 0)
		ball->reflectVX();
	else if ( x > xPos && x < (xPos + MAP_CELL_WIDTH) && y > yPos && y < yPos + MAP_CELL_HEIGHT / 4  && ball->getVY() > 0)
		ball->reflectVY();
	else if ( x > xPos && x < (xPos + MAP_CELL_WIDTH) && y > (yPos + (MAP_CELL_HEIGHT / 4) * 3) && y < (yPos + MAP_CELL_HEIGHT)  && ball->getVY() < 0)
		ball->reflectVY();
	else if ( x > (xPos + (MAP_CELL_WIDTH / 4) * 3) && x < xPos + MAP_CELL_WIDTH && (y > (yPos) && y < (yPos + MAP_CELL_HEIGHT))  && ball->getVX() < 0)
		ball->reflectVX();
	resistance--;
	setColor();
}

void Brick::setColor()
{
	blue = 255 - resistance * 20;
	while(blue < 0)
		blue += 255;
}

void Brick::crack()
{
	resistance = ZERO;
}