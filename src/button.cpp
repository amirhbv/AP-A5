#include "button.hpp"

using namespace std;

Button::Button(int _topCornerX, int _topCornerY, int _width, int _height, string _caption)
{
	topCornerX = _topCornerX;
	topCornerY = _topCornerY;
	width = _width;
	height = _height;
	caption = _caption;
}

void Button::show(Window *win, RGB color)
{
	win->fill_rect(topCornerX, topCornerY, width, height, color);
	win->show_text(caption, topCornerX + width / 2 - 6 * caption.size(), topCornerY + height / 2 - 15, WHITE, FONT_FREESANS, BUTTON_FONTSIZE);
}

bool Button::contains(int x, int y)
{
	return (x > topCornerX && x < topCornerX + width) && (y > topCornerY && y < topCornerY + height);
}