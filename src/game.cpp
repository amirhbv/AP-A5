#include "game.hpp"

using namespace std;

Game::Game(Window *_win)
{
	win = _win;
	score = ZERO;
	gameOver = FALSE;
	angle = 90;
	ballX = GAME_WINDOW_WIDTH / 2 - BALL_RADIUS / 2;
	ballY = GAME_TOP_MARGIN + (NUMBER_OF_ROWS + 1) * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN) - BALL_RADIUS;
	firePower = FALSE;
	isfirePowerOn = FALSE;
	for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
		for (int row = ZERO; row < NUMBER_OF_ROWS; row++)
			gameObjects[column][row] = NULL;
	srand(time(0));
	shoot = FALSE;
	mouseClick = FALSE;
	addBall();
	handleGameObject();
}

Game::~Game()
{
	// for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
	// 	for (int row = ZERO; row < NUMBER_OF_ROWS; row++)
	// 		if(gameObjects[column][row] != NULL)
	// 			delete gameObjects[column][row];
	// for (unsigned int i = ZERO; i < balls.size(); i++)
	// 	delete balls[i];
}

void Game::addBall()
{
	balls.push_back(new Ball());
	balls.back()->setPos(ballX, ballY);
}

string Game::toString(int n)
{
	stringstream ss;
	ss << n;
	return ss.str();
}

void Game::display(MyString *_playerId, int _highScore)
{
	playerId = _playerId;
	highScore = _highScore;
	while (!gameOver)
	{
		if (score > highScore)
			highScore = score;
		draw();
		handleLastEvent();
		if (shoot)
		{
			score++;
			if (firePower)
			{
				isfirePowerOn = TRUE;
				firePower = FALSE;
				shootFireBall();
			}
			else
			{
				isfirePowerOn = FALSE;
				shootBalls();
			}
			handleGameObject();
			shoot = FALSE;
		}
	}
}

void Game::draw()
{
	win->clear();
	drawHeader();
	drawGameObjects();
	drawBallAndArrow();
	win->update_screen();
}

void Game::drawHeader()
{
	// win->draw_bg(IMG_BACKGROUND, 0, 0);
	win->show_text("Name : " + playerId->getString(), GAME_LEFT_MARGIN * 2, HEADER_TOP_MARGIN, WHITE, FONT_FREESANS, BUTTON_FONTSIZE);
	win->show_text("Score : " + toString(score), GAME_LEFT_MARGIN * 2, HEADER_TOP_MARGIN * 2, WHITE, FONT_FREESANS, BUTTON_FONTSIZE);
	win->show_text("Record : " + toString(highScore), GAME_LEFT_MARGIN * 2, HEADER_TOP_MARGIN * 3, WHITE, FONT_FREESANS, BUTTON_FONTSIZE);
	win->draw_line(0, GAME_TOP_MARGIN, GAME_LEFT_MARGIN + GAME_WINDOW_WIDTH, GAME_TOP_MARGIN, RED);
	win->draw_line(0, GAME_TOP_MARGIN + 8 * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN), GAME_LEFT_MARGIN + GAME_WINDOW_WIDTH, GAME_TOP_MARGIN + 8 * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN), RED);
}

void Game::drawGameObjects()
{
	for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
		for (int row = ZERO; row < NUMBER_OF_ROWS; row++)
			if (gameObjects[column][row] != NULL)
				gameObjects[column][row]->draw(win, column, row);
}

void Game::drawBallAndArrow()
{
	if (!shoot)
	{
		win->draw_png(IMG_ARROW, ballX - ARROW_WIDTH / 2 + ARROW_HEIGHT / 2 - BALL_RADIUS / 2, ballY, ARROW_WIDTH, ARROW_HEIGHT, angle);
		if (firePower)
		{
			win->show_text("x1", ballX - BALL_RADIUS / 2, ballY + BALL_RADIUS + GAME_SPACE_BETWEEN / 2, CYAN, FONT_FREESANS, BUTTON_FONTSIZE);
			win->draw_png(IMG_FIRE_POWER, ballX, ballY, BALL_RADIUS, BALL_RADIUS);
		}
		else
		{
			win->draw_png(IMG_BALL, ballX - BALL_RADIUS / 2, ballY, BALL_RADIUS, BALL_RADIUS);
			win->show_text("x" + toString(balls.size()), ballX - 10, ballY + BALL_RADIUS + GAME_SPACE_BETWEEN / 2, CYAN, FONT_FREESANS, BUTTON_FONTSIZE);
		}
	}
	else
	{
		if (isfirePowerOn)
		{
			if (balls[0]->getState() == INSIDE)
				balls[0]->drawFireball(win);
		}
		else
			for (unsigned int i = ZERO; i < balls.size(); i++)
				if (balls[i]->getState() == INSIDE)
					balls[i]->draw(win);
	}
}

void Game::handleLastEvent()
{
	Event lastEvent = win->pollForEvent();
	if (lastEvent.type() == QUIT)
		gameOver = TRUE;
	if (lastEvent.type() == KEY_PRESS)
	{
		switch (lastEvent.pressedKey())
		{
		case ESC:
			gameOver = TRUE;
			break;
		case SPACE:
			shoot = TRUE;
			break;
		case RIGHT_ARROW:
			if (angle < ANGLE_MAXIMUM)
				angle += DELTA_TETA;
			break;
		case LEFT_ARROW:
			if (angle > ANGLE_MINIMUM)
				angle -= DELTA_TETA;
			break;
		}
	}
	if(lastEvent.type() == LCLICK)
		mouseClick = TRUE;
	if(lastEvent.type() == LRELEASE)
	{
		mouseClick = FALSE;
		shoot = TRUE;
	}
	if (mouseClick && lastEvent.type() == MMOTION)
	{
		int x = lastEvent.mouseX();
		int y = lastEvent.mouseY();
		float deltaX = ballX - x;
		float deltaY = ballY - y;
		if (!deltaX)
			angle = 90;
		else
		{
			float rad = atan(deltaY / deltaX);
			angle = Deg(rad) - 90;
			/*if(angle < ANGLE_MINIMUM)
				angle = ANGLE_MINIMUM;
			if(angle > ANGLE_MAXIMUM)
				angle = ANGLE_MAXIMUM;*/
		}
	}
}

void Game::handleGameObject()
{
	if (isGameOver())
		gameOver = TRUE;
	moveGameObjectsDown();
	addGameObjectToFirstRow();
}

bool Game::isGameOver()
{
	int row = NUMBER_OF_ROWS - 1;
	for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
		if (gameObjects[column][row] != NULL)
			if (gameObjects[column][row]->type() == MAP_BRICK || gameObjects[column][row]->type() == MAP_BOMB)
				return 1;
	return 0;
}

void Game::moveGameObjectsDown()
{
	for (int row = NUMBER_OF_ROWS - 1; row > 0; row--)
		for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
			gameObjects[column][row] = gameObjects[column][row - 1];
}

void Game::addGameObjectToFirstRow()
{
	int levelModFive = (score + 1) % 5;
	int row = ONE;
	if (levelModFive == 3)
	{
		int column = rand() % NUMBER_OF_COLUMNS;
		gameObjects[column][row] = new Brick(score + 1);
	}
	else if (levelModFive == 4)
	{
		int freeColumn = rand() % NUMBER_OF_COLUMNS;
		for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
			if (column != freeColumn)
				gameObjects[column][row] = new Brick(score + 1);
	}
	else
	{
		int column = rand() % NUMBER_OF_COLUMNS;
		gameObjects[column][row] = new Brick(score + 1);
		int secondColumn = rand() % NUMBER_OF_COLUMNS;
		while (secondColumn == column)
			secondColumn = rand() % NUMBER_OF_COLUMNS;
		gameObjects[secondColumn][row] = new Brick(score + 1);
	}
	addSpecialGameObjectsToFirstRow();
}

void Game::addSpecialGameObjectsToFirstRow()
{
	int number = numberOfFreeCellInFirstRow();
	int rnd = (rand() % number) + 1;
	int probability = rand() % 10;
	int row = ONE;
	for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
	{
		if (gameObjects[column][row] == NULL)
			rnd--;
		if (gameObjects[column][row] == NULL && !rnd)
		{
			if (probability == 0)
				gameObjects[column][row] = new Bomb();
			else if (probability == 1)
				gameObjects[column][row] = new FirePower();
			else if (probability < 5)
				gameObjects[column][row] = new LifeBall();
		}
	}
}

int Game::numberOfFreeCellInFirstRow()
{
	int counter = ZERO;
	int row = ONE;
	for (int column = ZERO; column < NUMBER_OF_COLUMNS; column++)
		if (gameObjects[column][row] == NULL)
			counter++;
	return counter;
}

void Game::shootFireBall()
{
	balls[0]->setPos(ballX + BALL_RADIUS / 2, ballY + BALL_RADIUS / 2, angle);
	balls[0]->setState(INSIDE);
	while (balls[0]->getState() == INSIDE)
	{
		Event lastEvent = win->pollForEvent();
		if (lastEvent.type() == QUIT)
			return;
		handleFireBallMove(balls[0]);
		Delay(TICK_DURATION);
	}
}

void Game::handleFireBallMove(Ball *ball)
{
	ball->move();
	draw();
	if (ball->getY() > GAME_TOP_MARGIN + (NUMBER_OF_ROWS + 1) * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN) || ball->getY() < GAME_TOP_MARGIN
	        || ball->getX() < GAME_LEFT_MARGIN || ball->getX() > GAME_LEFT_MARGIN + NUMBER_OF_COLUMNS * (MAP_CELL_WIDTH + GAME_SPACE_BETWEEN) )
	{
		ball->setState(OUTSIDE);
		return;
	}
	int column = calculateColumn(ball->getX());
	int row = calculateRow(ball->getY());
	if (row > 6 || gameObjects[column][row] == NULL)
		return;
	if (gameObjects[column][row]->type() == MAP_BRICK)
	{
		Brick *brick = dynamic_cast<Brick*> (gameObjects[column][row]);
		brick->crack();
	}
	else if (gameObjects[column][row]->type() == MAP_LIFE_BALL)
		addBall();
	else if (gameObjects[column][row]->type() ==  MAP_FIRE_POWER)
		firePower = TRUE;
	else if (gameObjects[column][row]->type() == MAP_BOMB)
		hitBomb(column, row);

	// delete gameObjects[column][row];
	gameObjects[column][row] = NULL;
}

void Game::shootBalls()
{
	for (unsigned int i = ZERO; i < balls.size(); i++)
	{
		balls[i]->setPos(ballX + BALL_RADIUS / 2, ballY + BALL_RADIUS / 2, angle);
		balls[i]->setState(INSIDE);
	}

	for (unsigned int i = ZERO; i < balls.size(); i++)
		for (unsigned int j = ZERO; j <= i; j++)
			handleBallMove(balls[j]);

	unsigned int numberOfBalls = balls.size();
	bool exit = FALSE;
	while (!exit)
	{
		exit = TRUE;
		Event lastEvent = win->pollForEvent();
		if (lastEvent.type() == QUIT)
			return;
		for (unsigned int i = ZERO; i < numberOfBalls; i++)
		{
			if (balls[i]->getState() == INSIDE)
			{
				exit = FALSE;
				handleBallMove(balls[i]);
				Delay(TICK_DURATION);
			}
		}
	}
}

void Game::handleBallMove(Ball* ball)
{
	ball->move();
	draw();
	reflectFromWalls(ball);
	int column = calculateColumn(ball->getX());
	int row = calculateRow(ball->getY());
	if (row > 6  || row < 0 || column < 0 || column > 4 || gameObjects[column][row] == NULL)
		return;

	if (gameObjects[column][row]->type() == MAP_BRICK)
	{
		Brick *brick = dynamic_cast<Brick*> (gameObjects[column][row]);
		brick->hitBrick(ball);
		if (!brick->getResistance())
			gameObjects[column][row] = NULL;
		return;
	}
	else if (gameObjects[column][row]->type() == MAP_LIFE_BALL)
		addBall();
	else if (gameObjects[column][row]->type() ==  MAP_FIRE_POWER)
		firePower = TRUE;
	else if (gameObjects[column][row]->type() == MAP_BOMB)
		hitBomb(column, row);

	// delete gameObjects[column][row];
	gameObjects[column][row] = NULL;
}

void Game::reflectFromWalls(Ball *ball)
{
	if (ball->getY() > GAME_TOP_MARGIN + (NUMBER_OF_ROWS + 1) * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN) )
		ball->setState(OUTSIDE);
	if (ball->getY() < GAME_TOP_MARGIN)
		ball->reflectVY();
	if (ball->getX() < GAME_LEFT_MARGIN || ball->getX() > GAME_LEFT_MARGIN + NUMBER_OF_COLUMNS * (MAP_CELL_WIDTH + GAME_SPACE_BETWEEN) )
		ball->reflectVX();
}

void Game::hitBomb(int column, int row)
{
	gameObjects[column][row] = NULL;
	for (int deltaX = -1; deltaX < 2; deltaX++)
		for (int deltaY = -1; deltaY < 2; deltaY++)
		{
			int currentColumn = column + deltaX;
			int currentRow = row + deltaY;
			if (currentRow < 0 || currentColumn < 0 ||
			        currentColumn >= NUMBER_OF_COLUMNS || currentRow >= NUMBER_OF_ROWS ||
			        gameObjects[currentColumn][currentRow] == NULL)
				continue;
			if (gameObjects[currentColumn][currentRow]->type() == MAP_BRICK)
			{
				Brick *brick = dynamic_cast<Brick*> (gameObjects[currentColumn][currentRow]);
				brick->crack();
			}
			else if (gameObjects[currentColumn][currentRow]->type() == MAP_BOMB)
				hitBomb(currentColumn, currentRow);

			// delete gameObjects[currentColumn][currentRow];
			gameObjects[currentColumn][currentRow] = NULL;
		}
}

int Game::calculateColumn(int x)
{
	return (x - GAME_LEFT_MARGIN) / ( MAP_CELL_WIDTH + GAME_SPACE_BETWEEN);
}

int Game::calculateRow(int y)
{
	return (y - GAME_TOP_MARGIN) / ( MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN );
}
