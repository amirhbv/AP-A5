#include "gameObject.hpp"

using namespace std;

int GameObject::calculateXPos(int column)
{
	return GAME_LEFT_MARGIN + column * (MAP_CELL_WIDTH + GAME_SPACE_BETWEEN);
}

int GameObject::calculateYPos(int row)
{
	return GAME_TOP_MARGIN + row * (MAP_CELL_HEIGHT + GAME_SPACE_BETWEEN);
}

string GameObject::toString(int n)
{
	stringstream ss;
	ss << n;
	return ss.str();
}