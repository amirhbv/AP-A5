#include "gameover.hpp"

using namespace std;

GameOver::GameOver(Window *_win)
{
	win = _win;
	addButtons();
	selectedButton = MENU;
	exit = FALSE;
}

void GameOver::addButtons()
{
	vector<string> captions;
	captions.push_back("New Game");
	captions.push_back("Menu");

	for (unsigned int i = ZERO ; i < captions.size(); i++)
		buttons.push_back(Button(LEFT_MARGIN, TOP_MARGIN + (i + 2) * (BUTTON_HEIGHT + SPACE_BETWEEN), BUTTON_WIDTH, BUTTON_HEIGHT, captions[i]));
}

int GameOver::display(int score, int record)
{
	while (!exit)
	{
		draw(score, record);
		handleLastEvent();
	}
	return chooseButton();
}

void GameOver::draw(int score, int record)
{
	win->clear();
	win->draw_bg(IMG_BACKGROUND, 0, 0);
	win->show_text("GAME OVER! ", LEFT_MARGIN / 2, TOP_MARGIN / 2, WHITE, FONT_FREESANS, GAMEOVER_FONTSIZE);
	win->draw_rect(LEFT_MARGIN / 2, TOP_MARGIN + SPACE_BETWEEN, BUTTON_WIDTH * 2, BUTTON_HEIGHT * 2, RED);
	win->show_text("Score : " + toString(score), LEFT_MARGIN, 1.5 * TOP_MARGIN + 15, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	win->show_text("Record : " + toString(score), LEFT_MARGIN - 10, 2 * TOP_MARGIN + 15, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	drawButtons();
	win->update_screen();
}

void GameOver::drawButtons()
{
	for (unsigned int i = ZERO; i < buttons.size(); i++)
	{
		if (i == selectedButton)
			buttons[i].show(win, SELECTED_BUTTON_COLOR);
		else
			buttons[i].show(win, BUTTON_COLOR);
	}
}

void GameOver::handleLastEvent()
{
	Event lastEvent = win->pollForEvent();
	if (lastEvent.type() == QUIT)
		exit = TRUE;
	if (lastEvent.type() == KEY_PRESS)
	{
		switch (lastEvent.pressedKey())
		{
		case ESC:
			exit = TRUE;
			break;
		case ENTER:
			exit = TRUE;
			break;
		case UP_ARROW:
			selectedButton--;
			selectedButton %= buttons.size();
			break;
		case DOWN_ARROW:
			selectedButton++;
			selectedButton %= buttons.size();
			break;
		}
	}
	if (lastEvent.type() == LRELEASE || lastEvent.type() == LCLICK || lastEvent.type() == MMOTION)
	{
		int x = lastEvent.mouseX();
		int y = lastEvent.mouseY();
		for (unsigned int i = ZERO; i < buttons.size(); i++)
			if (buttons[i].contains(x, y))
			{
				selectedButton = i;
				if (lastEvent.type() == LRELEASE)
					exit = TRUE;
			}
	}
}

int GameOver::chooseButton()
{
	switch (selectedButton)
	{
	case NEW_GAME:
		return NEW_GAME;
		break;
	default:
		return MENU;
		break;
	}
}

string GameOver::toString(int n)
{
	stringstream ss;
	ss << n;
	return ss.str();
}