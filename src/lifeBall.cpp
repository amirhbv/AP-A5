#include "lifeBall.hpp"

void LifeBall::draw(Window *win, int column, int row)
{
	xPos = calculateXPos(column);
	yPos = calculateYPos(row);
	win->draw_png(IMG_LIFE_BALL, xPos + MAP_CELL_WIDTH / 2 - BALL_RADIUS / 2, yPos + MAP_CELL_HEIGHT / 2 - BALL_RADIUS / 2, BALL_RADIUS, BALL_RADIUS);
}