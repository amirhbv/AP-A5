#include "menu.hpp"

using namespace std;

Menu::Menu()
{
	win = new Window(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE);
	playerId = new MyString("");
	addButtons();
	exit = FALSE;
	selectedButton = NEW_GAME;
	loadScores();
}

void Menu::addButtons()
{
	vector<string> captions;
	captions.push_back("New Game");
	captions.push_back("Scoreboard");
	captions.push_back("Setting");
	captions.push_back("Exit");

	for (unsigned int i = ZERO ; i < captions.size(); i++)
		buttons.push_back(Button(LEFT_MARGIN, TOP_MARGIN + i * (BUTTON_HEIGHT + SPACE_BETWEEN), BUTTON_WIDTH, BUTTON_HEIGHT, captions[i]));
}

void Menu::loadScores()
{
	ifstream fin;
	fin.open(SAVED_SCORE_FILENAME);
	string name;
	int score;
	while (fin >> name)
	{
		fin >> score;
		users.push_back(User(name, score));
	}
	fin.close();
}

Menu::~Menu()
{
	delete win;
	delete playerId;
	// delete scoreboard;
	// delete setting;
	// delete game;
	// delete gameOver;
}

void Menu::display()
{
	while (!exit)
	{
		handleLastEvent();
		draw();
		// Delay(TICK_DURATION);
	}
	saveScores();
}

void Menu::draw()
{
	win->clear();
	win->draw_bg(IMG_BACKGROUND, 0, 0);
	drawButtons();
	win->update_screen();
}

void Menu::drawButtons()
{
	for (unsigned int i = ZERO; i < buttons.size(); i++)
	{
		if (i == selectedButton)
			buttons[i].show(win, SELECTED_BUTTON_COLOR);
		else
			buttons[i].show(win, BUTTON_COLOR);
	}
}

void Menu::saveScores()
{
	ofstream fout;
	fout.open(SAVED_SCORE_FILENAME, ofstream::out | ofstream::trunc);
	for (unsigned int i = ZERO; i < users.size(); i++)
		fout << users[i].getId() << ' ' << users[i].getHighscore() << endl;
	fout.close();
}

void Menu::handleLastEvent()
{
	Event lastEvent = win->pollForEvent();
	if (lastEvent.type() == QUIT)
		exit = TRUE;
	if (lastEvent.type() == KEY_PRESS)
	{
		switch (lastEvent.pressedKey())
		{
		case ESC:
			exit = TRUE;
			break;
		case ENTER:
			chooseButton();
			break;
		case UP_ARROW:
			selectedButton--;
			selectedButton %= buttons.size();
			break;
		case DOWN_ARROW:
			selectedButton++;
			selectedButton %= buttons.size();
			break;
		}
	}
	if (lastEvent.type() == LRELEASE || lastEvent.type() == LCLICK || lastEvent.type() == MMOTION)
	{
		int x = lastEvent.mouseX();
		int y = lastEvent.mouseY();
		for (unsigned int i = ZERO; i < buttons.size(); i++)
			if (buttons[i].contains(x, y))
			{
				selectedButton = i;
				if (lastEvent.type() == LRELEASE)
					chooseButton();
			}
	}
}

void Menu::chooseButton()
{
	switch (selectedButton)
	{
	case NEW_GAME:
		newGame();
		break;
	case SCOREBOARD:
		showScoreboard();
		break;
	case SETTING:
		showSetting();
		break;
	case EXIT:
		exit = TRUE;
		break;
	}
}

void Menu::showScoreboard()
{
	sort(users.begin(), users.end());
	int numberOfUsers = min(SCOREBOARD_MAXIMUM_NUMBER_OF_USERS, int(users.size()));
	scoreboard = new Scoreboard(win);
	scoreboard->display(vector<User>(users.begin(), users.begin() + numberOfUsers));
	delete scoreboard;
}

void Menu::showSetting()
{
	setting = new Setting(win);
	setting->setUser(playerId);
	setting->display();
	std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(playerId->getString()));
	if (userIter != users.end())
		return;
	users.push_back(User(playerId->getString()));
}

void Menu::newGame()
{
	if (playerId->getString().empty())
		showSetting();
	if (playerId->getString().empty())
		return;
	game = new Game(win);
	game->display(playerId, playerHighScore());
	showGameOver(game->getScore());
}

void Menu::showGameOver(int score)
{
	if (score > playerHighScore())
		setPlayerHighScore(score);

	gameOver = new GameOver(win);
	int whatsNext = gameOver->display(score, playerHighScore());
	if (whatsNext == NEW_GAME)
		newGame();
}

int Menu::playerHighScore()
{
	std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(playerId->getString()));
	return userIter->getHighscore();
}

void Menu::setPlayerHighScore(int score)
{
	std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(playerId->getString()));
	userIter->setHighscore(score);
}