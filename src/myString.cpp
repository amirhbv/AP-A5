#include "myString.hpp"

using namespace std;

MyString::MyString(string input)
{
	inputString = input;
}

void MyString::eraseLastChar()
{
	if (inputString.size() > 0)
		inputString.erase(inputString.size() - 1);
}

void MyString::addChar(char c)
{
	if (inputString.size() < MAX_STRING_LENGTH)
		inputString += c;
}

string MyString::getString()
{
	return inputString;
}
