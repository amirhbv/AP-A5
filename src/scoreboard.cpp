#include "scoreboard.hpp"

using namespace std;

std::vector<Button> buttons;
unsigned int selectedButton;

Scoreboard::Scoreboard(Window *_win)
{
	win = _win;
	addButtons();
	exit = FALSE;
	selectedButton = MENU;
}

void Scoreboard::addButtons()
{
	buttons.push_back(Button(LEFT_MARGIN, TOP_MARGIN + 2 * SPACE_BETWEEN + BUTTON_HEIGHT * 4, BUTTON_WIDTH, BUTTON_HEIGHT, "Menu"));
}

void Scoreboard::display(vector<User> users)
{
	while (!exit)
	{
		draw(users);
		handleLastEvent();
	}
}

void Scoreboard::draw(vector<User> users)
{
	win->clear();
	win->draw_bg(IMG_BACKGROUND, 0, 0);
	win->show_text("Records : ", LEFT_MARGIN / 2, TOP_MARGIN, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	win->draw_rect(LEFT_MARGIN / 2, TOP_MARGIN + SPACE_BETWEEN, BUTTON_WIDTH * 2, BUTTON_HEIGHT * 4, RED);
	drawButtons();
	for (unsigned int i = ZERO; i < users.size();  i++)
	{
		win->show_text(users[i].getId(), LEFT_MARGIN / 2 + 20, TOP_MARGIN + SPACE_BETWEEN + (i + 1) * SPACE_BETWEEN_SCORES, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
		win->show_text(toString(users[i].getHighscore()), LEFT_MARGIN / 2 + BUTTON_WIDTH * 1.5, TOP_MARGIN + SPACE_BETWEEN + (i + 1) * SPACE_BETWEEN_SCORES, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	}
	win->update_screen();
}

void Scoreboard::drawButtons()
{
	for (unsigned int i = ZERO; i < buttons.size(); i++)
	{
		if (i == selectedButton)
			buttons[i].show(win, SELECTED_BUTTON_COLOR);
		else
			buttons[i].show(win, BUTTON_COLOR);
	}
}

void Scoreboard::handleLastEvent()
{
	Event lastEvent = win->pollForEvent();
	if (lastEvent.type() == QUIT)
		exit = TRUE;
	if (lastEvent.type() == KEY_PRESS)
		if (lastEvent.pressedKey() == ENTER || lastEvent.pressedKey() == ESC)
			exit = TRUE;
	if (lastEvent.type() == LRELEASE)
		for (unsigned int i = ZERO; i < buttons.size(); i++)
			if (buttons[i].contains(lastEvent.mouseX(), lastEvent.mouseY()))
				exit = TRUE;
}

string Scoreboard::toString(int n)
{
	stringstream ss;
	ss << n;
	return ss.str();
}