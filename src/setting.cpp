#include "setting.hpp"

using namespace std;

Setting::Setting(Window *_win)
{
	win = _win;
	addButtons();
	exit = FALSE;
	selectedButton = SAVE;
}

void Setting::addButtons()
{
	vector<string> captions;
	captions.push_back("Save");
	captions.push_back("Cancel");

	for (unsigned int i = ZERO ; i < captions.size(); i++)
		buttons.push_back(Button(LEFT_MARGIN, TOP_MARGIN * 3 + i * (BUTTON_HEIGHT + SPACE_BETWEEN), BUTTON_WIDTH, BUTTON_HEIGHT, captions[i]));
}

void Setting::display()
{
	oldUserName = playerId->getString();
	while (!exit)
	{
		draw();
		handleLastEvent();
	}
	chooseButton();
}

void Setting::draw()
{
	win->clear();
	win->draw_bg(IMG_BACKGROUND, 0, 0);
	win->show_text("Enter Username : ", LEFT_MARGIN / 2, TOP_MARGIN + SPACE_BETWEEN, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	win->draw_rect(LEFT_MARGIN / 2, TOP_MARGIN + 2 * SPACE_BETWEEN, BUTTON_WIDTH * 2, BUTTON_HEIGHT / 2, RED);
	if (playerId->getString().size())
		win->show_text(playerId->getString(), LEFT_MARGIN / 2 + 10, TOP_MARGIN + 2 * SPACE_BETWEEN, WHITE, FONT_FREESANS, SETTING_FONTSIZE);
	drawButtons();
	win->update_screen();
}

void Setting::drawButtons()
{
	for (unsigned int i = ZERO; i < buttons.size(); i++)
	{
		if (i == selectedButton)
			buttons[i].show(win, SELECTED_BUTTON_COLOR);
		else
			buttons[i].show(win, BUTTON_COLOR);
	}
}

void Setting::handleLastEvent()
{
	Event lastEvent = win->pollForEvent();
	if (lastEvent.type() == QUIT)
		exit = TRUE;
	if (lastEvent.type() == KEY_PRESS)
	{
		switch (lastEvent.pressedKey())
		{
		case ESC:
			exit = TRUE;
			break;
		case ENTER:
			exit = TRUE;
			break;
		case UP_ARROW:
			selectedButton--;
			selectedButton %= buttons.size();
			break;
		case DOWN_ARROW:
			selectedButton++;
			selectedButton %= buttons.size();
			break;
		case BACK_SPACE:
			playerId->eraseLastChar();
			break;
		default:
			playerId->addChar(lastEvent.pressedKey());
		}
	}
	if (lastEvent.type() == LRELEASE || lastEvent.type() == LCLICK || lastEvent.type() == MMOTION)
	{
		int x = lastEvent.mouseX();
		int y = lastEvent.mouseY();
		for (unsigned int i = ZERO; i < buttons.size(); i++)
			if (buttons[i].contains(x, y))
			{
				selectedButton = i;
				if (lastEvent.type() == LRELEASE)
					exit = TRUE;
			}
	}
}

void Setting::chooseButton()
{
	switch (selectedButton)
	{
	case SAVE:
		if (!playerId->getString().size())
			playerId->setString(oldUserName);
		break;
	case CANCEL:
		playerId->setString(oldUserName);
		break;
	}
}

