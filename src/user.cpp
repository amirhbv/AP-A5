#include "user.hpp"

using namespace std;

User::User(string _id, int _highscore)
{
	id = _id;
	highscore = _highscore;
}

void User::setHighscore(int _highscore)
{
	if (_highscore > highscore)
		highscore = _highscore;
}